extends KinematicBody2D

signal dialogo_iniziato

func _process(delta):
	var velocita = Vector2()
	var delta_speed = delta * 60
	
	if Input.is_key_pressed(KEY_A):
		velocita.x = -1
	
	if Input.is_key_pressed(KEY_D):
		velocita.x = 1
	
	var movimento = velocita.normalized() * 5 * delta_speed
	move_and_collide(movimento)
	animazioni(velocita)
	
	if $RayCast2D.is_colliding():
		var collisione = $RayCast2D.get_collider()
		if "Npc" in collisione.name and Input.is_key_pressed(KEY_E):
			set_process(false)
			emit_signal("dialogo_iniziato", collisione.testo)

func animazioni(velocita):
	if velocita.x == 1:
		$AnimatedSprite.play("Walk")
		$AnimatedSprite.flip_h = false
	
	elif velocita.x == -1:
		$AnimatedSprite.play("Walk")
		$AnimatedSprite.flip_h = true
	
	elif velocita == Vector2():
		$AnimatedSprite.play("Idle")

func _on_NinePatchRect_fine_dialogo():
	set_process(true)

extends NinePatchRect

signal fine_dialogo

func _ready():
	set_process(false)
	hide()

func _process(delta):
	if Input.is_key_pressed(KEY_SPACE):
		emit_signal("fine_dialogo")
		hide()

func _on_Player_dialogo_iniziato(testo):
	show()
	$Testo.text = tr(testo)
	set_process(true)